import EL from '../support/ids.js'
import CON from '../support/const.js'

const FORM = CON.FORM;

context('FORM | Talk to an expert', () => {
  beforeEach(() => {
    cy.visit('/fale-com-um-especialista/')
    cy.server();
    cy.route('POST', '**/api/v1/analytics/events').as('loading');
    cy.allow_cookie()
  })


  it('Assertions - Texts and fields', () => {
    //exemplo básico, mas o melhor é criar 'add commands' que irei fazer daqui em diante
    cy.wait('@loading').its('status').should('eq', 200);
    cy.main_title_is(FORM.PRINC)
    cy.sub_title_is(FORM.SUB)
    cy.get(EL.FORM.CONTACT).should('be.visible')
    cy.get(EL.FORM.HEADER).should('have.text', FORM.HEADER)

    cy.get(EL.FORM.CONTROL_FIELD).eq(0).should('have.attr', 'placeholder', FORM.EMAIL)
    cy.get(EL.FORM.CONTROL_FIELD).eq(1).should('have.attr', 'placeholder', FORM.NAME)
    cy.get(EL.FORM.CONTROL_FIELD).eq(2).should('have.attr', 'placeholder', FORM.PHONE)
    cy.get(EL.FORM.CONTROL_FIELD).eq(3).should('have.attr', 'placeholder', FORM.COMPANY)

    cy.get(EL.FORM.SELECT_FIELD).eq(1).should('contain.text', FORM.SECTOR)
    cy.get(EL.FORM.SELECT_FIELD).eq(2).should('contain.text', FORM.EMPLOYEES)
    cy.get(EL.FORM.SELECT_FIELD).eq(3).should('contain.text', FORM.BUY)
    cy.get(EL.FORM.AGREE).should('contain.text', FORM.AGREE)
  })

  it('Fill and selects - options form with constants', () => {
    //dessa forma, os valores preenchidos estão como constantes, no arquivo const.js. Também é possível deixar personalizável, irei fazer no proximo teste.
    cy.wait('@loading').its('status').should('eq', 200);
    cy.fill_persona_fields()
    cy.select_company_fields()
    cy.i_agree()
  })


  it('Fill and selects - options form with errors', () => {
    //mesmo teste anterior, mas com opções de variar a cada it, havendo reaproveitamento no commands.
    cy.wait('@loading').its('status').should('eq', 200);
    cy.fill_email('email@wrong').then(() => {
        cy.fill_name('N@m3 w1t4 3rr0r')
      })
      .then(() => {
        cy.fill_phone('n3mb3rs')
      })
      .then(() => {
        cy.fill_company('90898989')
      })
    cy.select_company_fields()
    cy.i_agree()
    cy.total_error(3)
  })
})
