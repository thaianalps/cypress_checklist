const content = {
    PERSONA: {
        EMAIL: 'thaianalps@gmail.com',
        NAME: 'Thaiana da Silva Lopes',
        COMPANY: 'Thaiana Ltda.',
        PHONE: '48984465359',
        SECTOR: 'ONG',
        EMPLOYEES: '1 - 10',
        BUY: 'Sim',

    },

    FORM: {
        PRINC: 'Fale com um especialista',
        SUB: 'ATENDIMENTO COMERCIAL',
        HEADER: 'Preencha seus dados abaixo e em breve entraremos em contato com você.',
        EMAIL: 'E-mail corporativo',
        NAME: 'Nome completo',
        PHONE: 'Telefone ou WhatsApp com DDD',
        COMPANY: 'Empresa',
        SECTOR: 'Escolha o setor',
        EMPLOYEES: 'Nº de funcionários',
        BUY: 'Gostaria de adquirir ($) um software de checklist?',
        AGREE: 'Eu concordo em receber comunicações.'
    },

}


export default content;
