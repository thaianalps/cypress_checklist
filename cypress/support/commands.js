// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import CON from '../support/const.js'
import EL from '../support/ids.js'
const PERSONA = CON.PERSONA;

Cypress.Commands.add('allow_cookie', () => {
    cy.get('.cc-ALLOW').click({
        force: true
    })
})

Cypress.Commands.add('main_title_is', text => {
    cy.get(EL.TITLES.PRINC).should('have.text', text)
})

Cypress.Commands.add('sub_title_is', text => {
    cy.get(EL.TITLES.SUB).should('have.text', text)
})

Cypress.Commands.add('fill_persona_fields', () => {
    cy.fill_email(PERSONA.EMAIL).then(() => {
        cy.fill_name(PERSONA.NAME).then(() => {
            cy.fill_phone(PERSONA.PHONE).then(() => {
                cy.fill_company(PERSONA.COMPANY)
            })
        })
    })
})

Cypress.Commands.add('fill_email', txt => {
    cy.get(EL.FORM.CONTROL_FIELD).eq(0).type(txt)
})

Cypress.Commands.add('fill_name', txt => {
    cy.get(EL.FORM.CONTROL_FIELD).eq(1).type(txt)
})
Cypress.Commands.add('fill_phone', txt => {
    cy.get(EL.FORM.CONTROL_FIELD).eq(2).type(txt)
})

Cypress.Commands.add('fill_company', txt => {
    cy.get(EL.FORM.CONTROL_FIELD).eq(3).type(txt)
})

Cypress.Commands.add('select_company_fields', () => {
    cy.get(EL.FORM.ITEMS).eq(1).then(($option) => {
        cy.get($option).select(PERSONA.SECTOR).then(() => {
            cy.get(EL.FORM.ITEMS).eq(2).then(($option) => {
                cy.get($option).select(PERSONA.EMPLOYEES).then(() => {
                    cy.get(EL.FORM.ITEMS).eq(3).then(($option) => {
                        cy.get($option).select(PERSONA.BUY)
                    })
                })
            })
        })
    })
})

Cypress.Commands.add('i_agree', () => {
    cy.get(EL.FORM.AGREE).click({
        force: true
    })
})

Cypress.Commands.add('total_error', num => {
    cy.get(EL.ERROR.INV_FIELD).should('have.length', num).then((feed) => {
        cy.log('Feedbacks messages: ', feed.text())
    })
})