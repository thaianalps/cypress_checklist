const elements = {
    TITLES: {
        PRINC: '.title',
        SUB: '.subtitle',
    },
    FORM: {
        CONTACT: '.contact-form',
        HEADER: '.contact-form > h3',
        CONTROL_FIELD: '.control-field',
        SELECT_FIELD: '.control-field-select',
        ITEMS: '.control-field-select-input',
        AGREE: '.control-field-checkbox',
    },
    ERROR: {
        INV_FIELD: '.control-error'
    }
}
export default elements;
